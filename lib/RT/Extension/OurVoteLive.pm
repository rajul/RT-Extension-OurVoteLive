package RT::Extension::OurVoteLive;
use strict;
use warnings;
 
use Exporter qw(import);

our $VERSION = '0.02';

our @EXPORT_OK = qw();

=head1 NAME

RT-Extension-OurVoteLive - The customizations that turn a RT into a RT for rt.ourvotelive.org

=head1 RT VERSION

Works with RT 4.2 and 4.4.

=head1 AUTHOR

Lawyers' Committee for Civil Rights Under LawE<lt>bduggan@lawyerscommittee.orgE<gt>

=head1 LICENSE AND COPYRIGHT

This software is Copyright (c) 2016 by Laywers' Committee for Civil Rights Under Law

This is free software, licensed under:

  The GNU General Public License, Version 2, June 1991

=cut

1;
